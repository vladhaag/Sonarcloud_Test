from src.core.arithmetic import Arithmetic


def test_add():
    assert Arithmetic.add(10, 5) == 15, "Should return the sum of two numbers"


def test_subtract():
    assert Arithmetic.subtract(10, 5) == 5, "Should return the difference of two numbers"
