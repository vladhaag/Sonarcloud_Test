class Arithmetic:
    @staticmethod
    def add(a, b):
        """Return the sum of two numbers."""
        return a + b

    @staticmethod
    def subtract(a, b):
        """Return the subtraction of two numbers, subtracting b from a."""
        return a - b
