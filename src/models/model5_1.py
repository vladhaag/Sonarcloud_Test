from src.util.global_imports import random
from src.core.source import Source
from src.core.server import Server
from src.core.sink import Sink
from src.util.simulations import run_simulation


def setup_model5_1(env):
    source = Source(env, "Source", (random.expovariate, 1 / 6))
    placement = Server(env, "Placement", (random.triangular, 3, 4, 5))
    inspection = Server(env, "Inspection", (random.uniform, 2, 4))
    good_parts = Sink(env, "Goodparts")
    bad_parts = Sink(env, "Badparts")

    source.connect(placement)
    placement.connect(inspection)
    inspection.connect(good_parts, 92)
    inspection.connect(bad_parts, 8)


def main():
    run_simulation(model=setup_model5_1, minutes=72000)


if __name__ == '__main__':
    main()
