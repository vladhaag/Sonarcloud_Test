# DMPG – Digital Model Play Ground

## Overview
DMPG (Digital Model PlayGround) is a framework for developing and executing discrete event simulations with support for advanced analysis, visualization, and optimization.

## Features (In Development)
- **Discrete Simulations**
- **Core Components:** Entity, Source, Server, Sink, Vehicle, Storage, Combiner, Separator, Tally Statistics
- **Replications & Multiprocessing**
- **Database Connection:** ORM-based results/statistics storage
- **Visualization:** Topology 2D
- **Processing Time Based on Entities and Servers**
- **Experiments & Distributed Simulations**
- **ML-Based Optimization**
- **Animated Simulations in Unreal Engine**
- **Analysis Functions:** Sensitivity analysis, steady-state analysis, traditional optimizations

**Version details:** See the `CHANGELOG`.

---

## Development Guidelines

<details><summary>Running flake8 Before Pushing</summary>

## Running flake8 Before Pushing

To ensure code quality and PEP 8 compliance, run `flake8` before committing your changes.

1. **Activate your Python virtual environment:**
   ```bash
   source venv/bin/activate  # macOS/Linux
   venv\Scripts\activate  # Windows
   ```

2. Run flake8:
   ```bash
   flake8
   ```
---
</details>

<details><summary>Connecting IntelliJ IDEA to SonarCloud via SonarLint</summary>

### Connecting IntelliJ IDEA to SonarCloud via SonarLint

SonarCloud is used for static analysis and tracking code quality metrics.

1. **Install SonarLint Plugin in IntelliJ IDEA**
   - Open Settings → Plugins
   - Search for SonarLint
   - Click Install and restart IntelliJ IDEA

2. **Configure SonarLint to Connect to SonarCloud**
   - Go to Settings → Tools → SonarLint
   - Under Connections, click ➕ → SonarCloud
   - Authenticate with your SonarCloud credentials
   - Select the corresponding SonarCloud Project for DMPG
   - Enable Automatic Mode to analyze files on-the-fly

3. **Run SonarLint Analysis**
   - Right-click the project folder in IntelliJ IDEA
   - Select Analyze with SonarLint
---
</details>

<details><summary>Using the Pylint Plugin in IntelliJ IDEA</summary>

### Using the Pylint Plugin in IntelliJ IDEA

Pylint helps maintain consistency and enforces Python best practices.

1. **Install the Pylint Plugin**

   - Open Settings → Plugins
   - Search for Pylint
   - Click Install and restart IntelliJ IDEA

2. **Configure Pylint for Your Project**
   - Open Settings → Languages & Frameworks → Pylint
   - Set the Pylint executable to:
     ```bash
     <your_virtualenv_path>/bin/pylint  # macOS/Linux
     <your_virtualenv_path>\Scripts\pylint.exe  # Windows
     ```
   - Add project-specific .pylintrc settings

3. **Run Pylint in IntelliJ IDEA**
   - Scan a Python file via the Plugin
   - Or analyze whole module via the Plugin

---
</details>






